# stephane.plus

## how to

### build from scratch

```console
$ git clone https://gitlab.com/stephane.tzvetkov/stephane.plus.git
$ cd stephane.plus
$ python -m venv .venv
$ source .venv/bin/activate
(.venv) $ pip install --upgrade pip
(.venv) $ pip install -r .requirements
(.venv) $ mkdocs serve -f .mkdocs.yml --dev-addr localhost:1993
```

### host with Nginx

⚠️ **Prerequisite**⚠️ : [Nginx](https://stephane-cheatsheets.readthedocs.io/en/latest/server/nginx/)

#### basic host server configuration

On the server hosting the project:
```console
(.venv) $ sudo mkdir /var/www/stephane.plus
(.venv) $ sudo chown your-user-name:your-nginx-group /var/www/stephane.plus
(.venv) $ mkdocs build --config-file .mkdocs.yml --site-dir /var/www/stephane.plus
(.venv) $ deactivate
$ sudo vi /etc/nginx/conf.d/stephane.plus.conf

  + > server {
  + >     listen 4242;
  + >     listen [::]:4242;
  + >     server_name localhost;
  + >
  + >     location / {
  + >         root   /var/www/stephane.plus;
  + >         index  index.html;
  + >     }
  + >
  + >     error_page 404 /404.html;
  + > }

$ sudo vi /etc/nginx/nginx.conf

    > ...
  + >     include /etc/nginx/conf.d/stephane.plus.conf
    > }

$ sudo nginx -t
```

Then [start (or reload if already started) the `nginx`
service](https://stephane-cheatsheets.readthedocs.io/en/latest/init-systems/init_systems/#basic-service-related-commands).

#### optional reverse proxy server configuration

Don't forget to also install
[Nginx](https://stephane-cheatsheets.readthedocs.io/en/latest/server/nginx/) on your proxy server.

On your proxy server:
```console
$ sudo vi /etc/nginx/conf.d/stephane.plus.conf

  + > server {
  + >     listen 80;
  + >     listen [::]:80;
  + >     server_name localhost;
  + >
  + >     location / {
  + >         proxy_pass http://123.123.1.1:4242;
  + >     }
  + > }

$ sudo vi /etc/nginx/nginx.conf

    > ...
  + >     include /etc/nginx/conf.d/stephane.plus.conf
    > }

$ sudo nginx -t
```

Then [start (or reload if already started) the `nginx`
service](https://stephane-cheatsheets.readthedocs.io/en/latest/init-systems/init_systems/#basic-service-related-commands).

